# **Container Solutions API Test Implementation Documentation**

## **Tech Stack used**

```js
NodeJs
ExpressJs
AWS RDS PostgreSQL as Database
Sequelize as ORM
Jest and Supertest for Testing Suite
```

## **Processes to start the server**

### ****Step 1****

* `Git Clone`

```bash
git clone https://gitlab.com/Eebru-gzy/API-Exercise.git
```

### ****Step 2****

* `Install dependencies`

```bash
npm install
```

### ****Step 3****

* `Set environment variables`

  * Use the `.envSample` file to set up the environment variables in a `.env` file in the root directory.

### ****Step 4****

* `Start Server`

```bash
npm start
```

## **The App**

* I used ExpressJs to bootstrap the NodeJs server, the app was initialized in the `app.js`  file but the server was initialized in the `index.js` file in the root directory of the application.

* The database config was set up in the `./db/db.config.js` file, it is then exported into the `index.js` file in the root directory, to be invoked when the server is started via `npm start`. I made the server to start only after the database is connected.

* The Database schema/model is in the `models` directory. After the database is connected, the schema would syncronize with the database to create the table and colums defined in the schema, after which the database would be seeded with the values in the `titanic.csv` file.

* The datatypes for all the database colums are `string` type.

* The API routes are in the `index.js` file in the `routes` directory.

* The data values seeded from the `titanic.csv` for the survived column in the database are `1` for the survived and `0` for the dead people, but the expected payload for the APIs are `true` or `false` for survived and dead respectively, so I updated the values to `true` and `false` in the `/people/convert` endpoint. NB: The subsequent survived column payload (and every other column payload) must be a `string` type.

## **The Endpoints:**

| HTTP VERB| API            | BODY | Description    |
| -------- | -------------- |------------|-----------------|
| GET      | /people/       | NONE | Gets all the records in the database|
| POST     | /people/       | Same as in [API.md](./API.md) file | Inserts record to the database|
| POST     | /people/convert| NONE | Converts 1 and 0 to true and false in the survived column|
| POST     | /people/findpersonbyname| {name: `<person name>`} | Finds a specific person by their name |
| GET      | /people/alldead| NONE  | Gets all the records of dead people |
| GET      | /people/allsurvived| NONE  | Finds all the records of survived people |
| GET      | /people/:id     | NONE  | Finds all the records of a specific person by their ID |
| PUT      | /people/:id     | Any property of the person   | Edits the given record of a specific person by their ID |
| DELETE   | /people/:id     | NONE  | Deletes all the records of a specific person by their ID |

## **Test Suites**

* I wrote some integration tests in the `__test__` directory, they basically test the 5 CRUD API that are assigned to me. To run test, run:

```bash
npm test
```

* ***Note:***  for the consistency of the database schema please comment out `line 62` of the `/model/people.js` before running the test.

## **Dockerization**

* To dockerize the application run:

```bash
npm run build
```

* This process would build the image of the application, while building, it would run the test and if everything goes well, it would tag the docker image as well as push it to my Docker hub repository.

* I have implemented multi-stage image build in the `Dockerfile`, also implemented skinny image build as much as possible by removing every files and folders that the image would not need to run from the image build context. This is done by mentioning the files and folders in the `.dockerignore` file.

## **Orchestration with Kubernetes**

* The kubernetes manifest files are resident in the `k8s` directory. I have a kubernetes `Deployment`, `NodePort Service`, `ConfigMap` and `Secret` objects defined which are all need to run the application. These objects are tested on a `minikube` cluster. To create these objects run;

```bash
kubectl apply -f k8s
```

* I also deployed this in a Google GKE cluster, this is the host link to the live API: `http://104.155.138.99/`

## **The End.**
