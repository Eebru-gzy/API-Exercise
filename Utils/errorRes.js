module.exports = (statusCode, error, res) => {
  return res.status(statusCode).json({
    success: false,
    error,
  });
};
