#------Base Image---------
FROM node:16-alpine AS base

WORKDIR /app

COPY package.json .

# ---- Dependencies ----
FROM base AS dependencies

RUN npm install --only=production 

# ---- Test ----
FROM base AS test

RUN npm install

COPY . .

RUN  npm test

# ----Final Release ----
FROM dependencies AS release

COPY . .

EXPOSE 9009

CMD ["npm", "start"]